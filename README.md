# Kafka Connect FileSystem Connector

This is a fork of original kafka-connect-fs [Kafka Connector](https://kafka.apache.org/documentation.html#connect) for reading records from files in the file systems specified and load them into Kafka.

Original documentation for this connector can be found [here](https://kafka-connect-fs.readthedocs.io/) and kafka-connect-fs repository is available [here](https://github.com/mmolimar/kafka-connect-fs).

## Development

To build a development version you'll need a recent version of Kafka. You can build
kafka-connect-fs with Maven using the standard lifecycle phases.

You could also build new jar package by just using Docker BuildKit:

```
bash > DOCKER_BUILDKIT=1 docker build --file Dockerfile.build --output dist .

PS > $env:DOCKER_BUILDKIT=1; docker build --file Dockerfile.build --output dist .; Remove-Item Env:\DOCKER_BUILDKIT
```

## FAQ

Some frequently asked questions on Kafka Connect FileSystem Connector can be found here -
https://kafka-connect-fs.readthedocs.io/en/latest/faq.html

## Contribute back to original project

- Source Code: https://github.com/mmolimar/kafka-connect-fs
- Issue Tracker: https://github.com/mmolimar/kafka-connect-fs/issues

## License

Released under the Apache License, version 2.0.
